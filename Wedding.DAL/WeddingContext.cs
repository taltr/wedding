﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Wedding.Model;

namespace Wedding.DAL
{
    public class WeddingContext : DbContext
    {
        public DbSet<Greeting> Greetings { get; set; }
        public DbSet<Guest> Guests { get; set; }
        public DbSet<Meal> Meals { get; set; }
        public DbSet<Party> Parties { get; set; }
    }
}
