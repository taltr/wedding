﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Wedding.Model
{
    public class Greeting
    {
        [Key]
        public int GreetingID { get; set; }

        [Required]
        public string GreetingText { get; set; }

        public virtual List<Party> Parties { get; set; }
    }
}
