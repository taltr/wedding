﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Wedding.Model
{
    public class Meal
    {
        [Key]
        public int MealID { get; set; }

        [Required]
        public string TypeOfMeal { get; set; }

        public virtual List<Guest> Guests { get; set; }
    }
}
