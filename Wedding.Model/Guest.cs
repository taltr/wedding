﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Wedding.Model
{
    public class Guest
    {
        [Key]
        public int GuestID { get; set; }

        [Required]
        public int MealID { get; set; }
        public virtual Meal Meal { get; set; }

        [Required]
        public int PartyID { get; set; }
        public virtual Party Party { get; set; }
    }
}
