﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Wedding.Model
{
    public enum ArrivalStatus { No, Yes, Maybe }
    public class Party
    {
        [Key]
        public int PartyID { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string LastName { get; set; }

        public ArrivalStatus? Status { get; set; }

        [Range(0,10)]
        public int? NumOfGuests { get; set; }

        [Required]
        public string Code { get; set; }

        public string Song { get; set; }

        public string Phone { get; set; }

        public string Category { get; set; }

        public string Host { get; set; }

        [Required]
        public bool IsSelfAdded { get; set; }

        [Required]
        public int GreetingID { get; set; }
        public virtual Greeting Greeting { get; set; }

        public virtual List<Guest> Guests { get; set; }
    }
}
