﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wedding.MvcUI.Models
{
    public class LoginVM
    {
        public string Password { get; set; }
        public string Message { get; set; }
    }
}