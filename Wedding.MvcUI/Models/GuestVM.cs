﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Wedding.Model;
using System.ComponentModel.DataAnnotations;
using Wedding.BL;

namespace Wedding.MvcUI.Models
{
    public class GuestVM
    {
        public int ID { get; set; }

        [Required(ErrorMessage="אנא בחר סוג מנה")]
        public int MealID { get; set; }

        public Guest GetGuest()
        {
            PartyManager manager = new PartyManager();
            Guest guest = new Guest()
            {
                GuestID = ID,
                Meal = manager.GetMealById(MealID)
            };
            return guest;
        }

        public static List<Guest> ConvertToDbGuests(List<GuestVM> guestsVM, int partyId)
        {
            if (guestsVM != null)
            {
                List<Guest> guests = new List<Guest>();
                foreach (var guestVM in guestsVM)
                {
                    guests.Add(new Guest() { MealID = guestVM.MealID, PartyID = partyId });
                }
                return guests;
            }
            return null;
        }

        public static List<GuestVM> ConvertToGuestsVM(List<Guest> guests)
        {
            List<GuestVM> guestsVM = new List<GuestVM>();
            foreach (var guest in guests)
            {
                guestsVM.Add(new GuestVM() { ID = guest.GuestID, MealID = guest.MealID });
            }
            return guestsVM;
        }
    }
}