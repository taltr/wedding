﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wedding.MvcUI.Models
{
    public class MealVM
    {
        public string Name { get; set; }
        public int Number { get; set; }
    }
}