﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Wedding.Model;
using System.ComponentModel.DataAnnotations;
using Wedding.BL;

namespace Wedding.MvcUI.Models
{
    public class PartyVM : IValidatableObject
    {
        public int PartyID { get; set; }

        public string Name { get; set; }

        [Required(ErrorMessage="אנא בחרו סטטוס הגעה")]
        public ArrivalStatus? Status { get; set; }

        [Range(0, 10)]
        public int? NumOfGuests { get; set; }

        public List<GuestVM> Guests { get; set; }

        public string Song { get; set; }

        public string Code { get; set; }

        public int GreetingID { get; set; }
        public Greeting Greeting { get; set; }

        public List<Meal> Meals { get; set; }

        public bool IsSelfAdded { get; set; }

        public string ErrorMessage { get; set; }

        public PartyVM()
        {
            PartyManager manager= new PartyManager();
            Meals = manager.GetAllMeals();
        }

        public Party GetParty()
        {
            Party party = new Party() {
            PartyID = PartyID,
            Code = Code,
            GreetingID = GreetingID,
            Name = Name,
            NumOfGuests = NumOfGuests,
            Song = Song,
            Status = Status,
            Guests = new List<Guest>()
            };

            foreach (GuestVM gst in Guests)
            {
                party.Guests.Add(gst.GetGuest());
            }
            return party;
        }

        //public void ConvertMeals()
        //{
        //    foreach (var guest in Guests)
        //    {
        //        guest.Meal = Meals.Where(m => m.MealID == guest.Meal.MealID).SingleOrDefault();
        //    }
        //}

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> errors = new List<ValidationResult>();
            if (Status != ArrivalStatus.No && NumOfGuests == null)
            {
                errors.Add(new ValidationResult("שכחת לבחור מספר מוזמנים"));
            }
            return errors;
        }
    }
}