﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Wedding.Model;

namespace Wedding.MvcUI.Models
{
    public class ManageVM
    {
        public List<Party> Parties { get; set; }

        public List<MealVM> Meals { get; set; }

        public int StatusArriving { get; set; }
        public int StatusNotArriving { get; set; }
        public int StatusMaybe { get; set; }
        public int StatusPending { get; set; }
        
        public ManageVM()
        {
            Parties = new List<Party>();
            Meals = new List<MealVM>();
        }

    }

   
}