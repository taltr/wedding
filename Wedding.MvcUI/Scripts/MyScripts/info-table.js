﻿$(function () {
    $('.not-coming').closest('tr').addClass('danger');
    $('.coming').closest('tr').addClass('success');
    $('.maybe-coming').closest('tr').addClass('warning');
    $('.self-added').parent().addClass('purple-bg');
});