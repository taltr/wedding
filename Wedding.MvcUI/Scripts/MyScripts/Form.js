﻿function activateStatusButton(status) {
    if (status == "Maybe") {
        $('#lbl-maybe').addClass('active');
        $('#radio-maybe').prop('checked', true);
    }
    else if (status == "Yes") {
        $('#lbl-yes').addClass('active');
        $('#radio-yes').prop('checked', true);
    }
    else if (status == "No") {
        $('#lbl-no').addClass('active');
        $('#radio-no').prop('checked', true);
    }
}

function showGuests(numOfGuests) {
    if (numOfGuests != null && numOfGuests > 0) {
        $('#num-of-guests-block').removeClass('hidden');
        var numOfGuests = $('#num-of-guests').val();
        var count = 1;
        while (count <= numOfGuests) {
            $('.guest:nth-child(' + count + ')').removeClass('hidden');
            count++;
        }
    }
}

function showGuestsByNumOfGuests() {
    $('.guest').addClass('hidden');
    var numOfGuests = $('#num-of-guests').val();
    if (numOfGuests != null) {
        var count = 1;
        while (count <= numOfGuests) {
            $('.guest:nth-child(' + count + ')').removeClass('hidden');
            count++;
        }
    }
}

function fillMeals() {
    var numOfGuests = $('#num-of-guests').val();
    var count = 0;
    var mealCount = 1;
    while (count < numOfGuests) {
        $('#Guests_' + count + '__MealID').val($('#meals-hidden input:nth-child(' + mealCount + ')').val());
        count++;
        mealCount++;
    }
}


$(function () {
    if ($('#status-hidden').val() != null && $('#status-hidden').val() != "") {
        $('#hide-form-box').removeClass('hidden');
        $('#info-to-update').addClass('hidden');
        activateStatusButton($('#status-hidden').val());
        showGuests($('#num-of-guests').val());
        if ($('#status-hidden').val() == "Yes" || $('#status-hidden').val() == "Maybe") {
            fillMeals()
            $('#song-block').removeClass('hidden');
        }
    }

    $('#hide-form-btn').click(function () {
        $('#info-to-update').removeClass('hidden');
        $(this).parent().addClass('hidden');
    });


    $('input:radio').change(function () {
        if ($('#lbl-maybe').hasClass('active') || $('#lbl-yes').hasClass('active')) {
            $('#num-of-guests-block').removeClass('hidden');
            showGuestsByNumOfGuests();
            $('#song-block').removeClass('hidden');
        }
            //if ($('#radio-yes').is(':checked') || $('#radio-maybe').is(':checked')) {
            //    $('#num-of-guests-block').removeClass('hidden');
            //    $('.guest:first-child').removeClass('hidden');
            //    $('#song-block').removeClass('hidden');
            //}
        else {
            $('#num-of-guests').val(null);
            $('#num-of-guests-block').addClass('hidden');
            $('.guest').addClass('hidden');
            $('.guest select').val("");
            $('#song-block input').val("");
            $('#song-block').addClass('hidden');
        }
    });

    $('#num-of-guests').change(function () {
        showGuestsByNumOfGuests();
    });

   
    $('#update-party').click(function (e, options) {
        options = options || {};

        if (!options.done_changes) {

            e.preventDefault();
            if ($('#num-of-guests').val() == null || $('#num-of-guests').val() == "") {
                $('.guest').remove();
            }
            else if ($('#num-of-guests').val() > 0) {
                var count = parseInt($('#num-of-guests').val(), 10) + 1;
                while (count <= 10) {
                    $('.guest:nth-child(' + count + ')').remove();
                    count++;
                }
            }
            $(e.currentTarget).trigger('click', { 'done_changes': true });
        }
    });

    $("#btnCreateParty").click(function (event) {
        var hasErrors = false;
        // if first input is empty
        if ($('#txtName').val() == null || $('#txtName').val() == "") {
            // add "has-error" class
            $('#redirect-name').addClass('has-error');
            hasErrors = true;
        }
        // if second input is empty
        if ($('#txtPhone').val() == null || $('#txtPhone').val() == "") {
            // add "has-error" class
            $('#redirect-phone').addClass('has-error');
            hasErrors = true;
        }
        if (hasErrors)
            event.preventDefault();
    });

    $("#btnRedirectWithCode").click(function (event) {
        // if code input is empty
        if ($('#txtCode').val() == null || $('#txtCode').val() == "") {
            // add "has-error" class
            $('#redirect-by-code').addClass('has-error');
            event.preventDefault();
        }
    });

});