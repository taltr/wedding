﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Wedding.Model;
using Wedding.BL;
using System.Configuration;
using Wedding.MvcUI.Models;
using System.Collections;

namespace Wedding.MvcUI.Controllers
{
    public class ManageController : Controller
    {
        public string Message { get; set; }
     
        public ActionResult Index()
        {
            // Get the id from the url:
            var pass = "";
            var urlSplit = Request.RawUrl.Split(new[] { "manage/" }, StringSplitOptions.RemoveEmptyEntries);
            if (urlSplit.Length == 2)
            {
                pass = urlSplit[1];
            }
            if (!string.IsNullOrWhiteSpace(pass))
            {
                return Index(pass);
            }

            Message = "";
            return View("Login", new LoginVM { Message = Message });
        }

        [HttpPost]
        public ActionResult Index(string Password)
        {
            var PASSWORD = ConfigurationManager.AppSettings["AdminPassword"].ToString();

            if (PASSWORD == Password)
            {
                PartyManager manager = new PartyManager();

                var manageVM = new ManageVM();

                manageVM.Parties = manager.GetPartyList();

                // Count the arriving
                foreach (var party in manageVM.Parties.Where(party => party.Status == ArrivalStatus.Yes))
                    manageVM.StatusArriving += party.NumOfGuests ?? 0;
                
                foreach (var party in manageVM.Parties.Where(party => party.Status == ArrivalStatus.Maybe))
                    manageVM.StatusMaybe += party.NumOfGuests ?? 0;

                // The invitations that selected "not arriving" (therfore there is no number of guests)
                manageVM.StatusNotArriving = manageVM.Parties.Count(party => party.Status == ArrivalStatus.No);

                // The pending invitations (people who did not fill the form
                manageVM.StatusPending += manageVM.Parties.Count(party => !party.Status.HasValue);

                // count the orders of each meal 
                var allMealTypes = manager.GetAllMeals();

                foreach (var meal in allMealTypes)
                {
                    var countMealsOfType = 0;
                    foreach (var party in manageVM.Parties)
                    {
                        // Get the number of meals from this type in this party
                        countMealsOfType += party.Guests.Where(g => g.Meal.MealID == meal.MealID).Count();
                    }

                    // Add the total of this meal type 
                    manageVM.Meals.Add(new MealVM { Name = meal.TypeOfMeal, Number = countMealsOfType });
                }

                return View("Index", manageVM);
            }
            Message = "הסיסמה שגויה";
            return View("Login", new LoginVM { Message = Message });

        }
    }
}
