﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Wedding.BL;
using Wedding.Model;
using Wedding.MvcUI.Models;

namespace Wedding.MvcUI.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index(string id)
        {
            PartyManager manager = new PartyManager();
            //manager.test();

            if (id != null)
            {
                Party party = manager.GetPartyByCode(id);
                if (party==null)
                {
                    return View(new PartyVM() { ErrorMessage = "הכתובת שהזנת אינה תקינה" });
                }
                PartyVM partyVM = new PartyVM() { Code = party.Code, Name = party.Name, PartyID = party.PartyID, Greeting = party.Greeting, GreetingID = party.GreetingID, IsSelfAdded= party.IsSelfAdded };
                if (party.Status!=null)
                {
                    partyVM.Status = party.Status;
                    partyVM.Guests = GuestVM.ConvertToGuestsVM(party.Guests);
                    partyVM.NumOfGuests = party.NumOfGuests;
                    partyVM.Song = party.Song;
                }
                return View(partyVM);
            }
            else
            {
                // There is no id provided
                return View(new PartyVM() { ErrorMessage =  "בדוק שהכתובת מלאה" });
            }
         
        }

        

        [HttpPost]
        public ActionResult Confirmation(PartyVM partyVM)
        {
            PartyManager manager = new PartyManager();
            if (ModelState.IsValid)
            {
                Party party = manager.GetPartyById(partyVM.PartyID);
                if (party != null)
                {
                    manager.DeleteGuestsOfParty(party.PartyID);
                    party.NumOfGuests = partyVM.NumOfGuests;
                    party.Song = partyVM.Song;
                    party.Status = partyVM.Status;
                    party.Guests = GuestVM.ConvertToDbGuests(partyVM.Guests, partyVM.PartyID);
                }
                bool success = manager.UpdatePartyStatus(party);
                if (success)
                {
                    bool isComing = true;
                    if (party.Status == ArrivalStatus.No)
                    {
                        isComing = false;
                    }
                    ViewBag.Code = party.Code;
                    return View(isComing);
                }
            }
            partyVM.Greeting = manager.GetGreeting(partyVM.GreetingID);
            return View("Index", partyVM);
        }


        [HttpPost]
        public ActionResult CorrectCode(string code)
        {
            return RedirectToAction("", new { id = code });
        }
       
        
        public ActionResult DownloadFile()
        {
            var fileName = ConfigurationManager.AppSettings["PartyFileName"].ToString() + "_"+ DateTime.Now.ToString("yyyy_MM_dd_HHmmss") +".csv";
            var pathToPartyCsvFile = ConfigurationManager.AppSettings["pathToPartyCsvFile"].ToString();
            PartyManager manager = new PartyManager();
            var dtAllParties = manager.GetDataAsDataTable<Party>(manager.GetPartyList());
            CsvManager.CreateCSVFromGenericList(manager.GetPartyList(), pathToPartyCsvFile + fileName);

            string filepath = pathToPartyCsvFile + fileName; //  AppDomain.CurrentDomain.BaseDirectory + "C://" + filename;
            byte[] filedata = System.IO.File.ReadAllBytes(filepath);
            string contentType = MimeMapping.GetMimeMapping(filepath);

            var cd = new System.Net.Mime.ContentDisposition
            {
                FileName = fileName,
                Inline = true,
            };

            Response.AppendHeader("Content-Disposition", cd.ToString());

            return File(filedata, contentType);
        }

        [HttpPost]
        public ActionResult CreateNewParty(string name, string phone)
        {
            PartyManager manager = new PartyManager();
            string code = manager.GenerateCode();
            manager.CreateParty(name, code, phone);
            return RedirectToAction("", new { id = code });
        }

    }
}
