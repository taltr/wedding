﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wedding.DAL;
using Wedding.Model;

namespace Wedding.BL
{
    public class PartyManager : IDisposable
    {
        WeddingContext context = new WeddingContext();
        public void Dispose()
        {
            context.Dispose();
        }

        public void CreateParty(string name, string code, string phone)
        {
            Party newParty = new Party()
            {
                Name = name,
                GreetingID = 1,
                Code = code,
                Phone = phone,
                IsSelfAdded = true,
                LastName = "לא הוגדר"
            };
            AddPartyToDb(newParty);
        }

        public void AddPartyToDb(Party party)
        {
            try
            {
                context.Parties.Attach(party);
                context.Entry(party).State = EntityState.Added;
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public Party GetPartyByCode(string code)
        {
            return context.Parties.Where(p => p.Code.ToLower() == code.ToLower()).SingleOrDefault();
        }

        public Party GetPartyById(int id)
        {
            return context.Parties.Find(id);
        }

        public void DeleteGuestsOfParty(int partyId)
        {
            IEnumerable<Guest> guestsToRemove = context.Guests.Where(g=>g.PartyID==partyId).ToList();
            context.Guests.RemoveRange(guestsToRemove);
            context.SaveChanges();
        }

        //public void ResetGuestsInParty(int partyId)
        //{
        //    var party = GetPartyById(partyId);
        //    if (party != null)
        //    {
        //        party.Guests = new List<Guest>();
        //    }
        //}

        //public List<Guest> CreateGuestsInParty(List<GuestVM> partyGuests)
        //{
        //    List<Guest> guests = new List<Guest>();
        //    guests.Add(new Guest(){ GuestID= party.}

        //}

        public bool UpdatePartyStatus(Party party)
        {
            try
            {
                context.Parties.Attach(party);
                context.Entry(party).State = EntityState.Modified;
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public List<Meal> GetAllMeals()
        {
            return context.Meals.ToList();
        }

        //public void test()
        //{
        //    context.Parties.Where(p => p.Name == "tal");
        //}

        public Meal GetMealById(int mealId)
        {
            return context.Meals.Find(mealId);
        }


        public List<Party> GetPartyList()
        {
            return context.Parties.ToList();
        }

        public DataTable GetDataAsDataTable<T>(List<T> data)
        {
            PropertyDescriptorCollection properties =
            TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;
        }

        public bool CheckUniqueCode(string code)
        {
            bool uniqueCode = true;
            Party party = context.Parties.Where(p => p.Code.ToLower() == code.ToLower()).SingleOrDefault();
            if (party != null)
            {
                uniqueCode = false;
            }
            return uniqueCode;
        }

        public string GenerateCode()
        {
            bool uniqueCode = false;
            Guid newGuid;
            string code = "";
            while (!uniqueCode)
            {
                newGuid = Guid.NewGuid();
                code = newGuid.ToString().Substring(0, 4);
                uniqueCode = CheckUniqueCode(code);
            }
            return code;
        }

        public Greeting GetGreeting(int greetingId)
        {
            return context.Greetings.Find(greetingId);
        }
    }
}
